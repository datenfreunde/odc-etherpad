# pad.opendatacity.de

This repo contains files and documentation for pad.opendatacity.de.

We make this instance of [etherpad](http://etherpad.org/) available as a service for the community. It 
is widely used by hackathons etc. and should allways run under pad.opendatacity.de.


### Transition

Currently running on nyx.opendatacity.de, in the process of being migrated to urd.opendatacity.de.
